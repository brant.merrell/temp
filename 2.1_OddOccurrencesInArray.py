def OddOccurrencesInArray(A):
	import collections
	Odd = [m for m, count in collections.Counter(A).items() if not count % 2 == 0]
	return(Odd[0])

