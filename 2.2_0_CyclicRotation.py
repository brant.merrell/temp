
def CyclicRotation(A, K):
	if 0 < len(A) :
		N = 0
		while N < K:
			A = [A[ len(A)-1 ]] + A[0:(len(A)-1)]
			N = N + 1
	return(A)
