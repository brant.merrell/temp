def binary_gap(N):
	B = str(bin(N))
	import re
	B = re.sub("0b","", B)
	gp = re.sub("^0+|0+$","", B).split("1")
	for i in range(0, len(gp), 1): gp[i] = gp[i].count("")-1
	return(max(gp))
